## NCGDMW Lua Edition Frequently Asked Questions

#### Q) How can I make my mod compatible with NCGDMW Lua Edition?

A) Generally speaking, NCGDMW Lua Edition is compatible with most things out of
   the box.

   I have seen mods in the wild that modify base attributes and/or skills to
   provide buffs and etc, this will cause level ups/downs as those buffs are
   added/removed. This can be somewhat jarring for NCGDMW Lua Edition users.

   One way to do this that is more friendly to NCGDMW Lua Edition is to use a
   "Curse" instead of an "Ability" for your spell type. A Curse affects stats
   as a modifier and will be ignored by NCGDMW Lua Edition's calulations,
   whereas an Ability will modify the base stat.

   If you're writing a Lua mod, you can refer to the "Events & Interface"
   documentation (included with every NCGDMW Lua Edition zip file or
   [on the website](https://modding-openmw.gitlab.io/ncgdmw-lua/events_and_interface/))
   for information about how to hook in from the Lua side of things.

#### Q) How does this mod work? Why does it do what it does?

A) Please see [the README](/readme/#how-it-works) for a high-level explanation.

#### Q) (Insert Birthsign that modifies stats) doesn't behave like it should!

A) Prior to OpenMW 0.48, these kinds of birthsign effects weren't applied to the 
   base stat but as a modifier, which worked properly with the NCGD patterns.
   In OpenMW 0.48 and up, the engine now applies them to the base stat as is
   done in Morrowind.exe. This unfortunately broke how NCGD worked with these
   birthsigns.

   As of the NCGDMW Lua Edition 2.0 release, two solutions are offered:

   The first solution is a patch to vanilla birthsigns that re-implements them
   as curses instead of abilities (more info on that above). This should be
   used with OpenMW 0.48.

   Unfortunately, taking this approach breaks the birthsigns UI for converted
   signs.

   The second solution is to simply use OpenMW 0.49, in that version the
   birthsigns are fixed via Lua.

#### Q) How does this compare/relate to the original version on Nexus Mods?

A) This is a continuation of [the original NCGDMW by Greywander](https://www.nexusmods.com/morrowind/mods/44967),
   which is no longer supported and doesn't work with modern versions of
   OpenMW.

   That one was last updated in 2018, and since then changes were made to
   OpenMW which caused breakage in the mod since it unfortunately relied on
   "features" that were actually implementation inaccuracies (aka bugs).

   Atahualpa and I kept the mod going in the face of breakage (you can find
   that version [here](https://modding-openmw.com/mods/natural-character-growth-and-decay-mw/)),
   but during the development of OpenMW 0.48 it got to the point where there
   was no easy way to keep the mod working. Changes in that version make magic
   work more like Morrowind.exe, but unfortunately break assumptions made by
   this mod.

   Thanks to the efforts of many individuals that worked on OpenMW-Lua, I was
   able to port the vast majority of the mod's functionality over to a new
   version that's written with Lua. This version lacks the mastery mechanic
   which allowed skills and attributes to level up past 100, but it sports
   all new features including:

* Level up and decay progress are now visible via a dedcated NCGD stats menu
* The mod itself is made with far fewer lines of code (thanks to Lua features)
* Works with OpenMW 0.48 (and the related changes to magic which make it more
  like Morrowind.exe) and will be supported for future versions
* A Lua interface is provided for other mods to interoperate with this one (see
  the next question below for more information)
* It will also work with any other mod or script that happens to alter
  attributes outside of this mod's control (by applying those changes on top of
  our own calculations)
* ... And more to be added in the future!

#### Q) My level progress isn't increasing, when will I level up?

A) At this time, mods can't change the base game user interface, and NCGD
   zeroes out the vanilla level progress.

   The NCGD Stats Menu will show your level and decay progress if that feature
   is enabled. To use the menu, set it to a key using the mod settings menu.

#### Q) When will decay kick in? I've played for many hours/waited for hundreds of days and nothing happened!

A) Even with the "fast" setting, decay takes a long time to kick in. Increasing
   your attributes (done by increasing skills) will significantly affect decay
   progress, which of course doesn't happen while just standing there which
   means you could potentially wait for thousands of days before seeing a decay
   that way.

#### Q) Why can't I raise attributes and skills past 100?

A) A companion mod implements this: [MBSP Uncapped (OpenMW Lua) - NCGD Compat](https://www.nexusmods.com/morrowind/mods/53064).
