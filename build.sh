#!/bin/sh
set -e

for d in "00 Core" "01 ClassicNCGD" "02 NewNCGD" "03 NewNCGDDoubleSkillBonuses" '04 AltStart (For Total Conversions)' "05 Starwind"; do
    cd "$d"
    delta_plugin convert *.yaml || echo FAILED
    cd ..
done

cd "06 Patches"

for d in AbandonedFlatNCGDMWLuaPatch.d \
             HavishNCGDMWLuaPatch.d \
             MeteorNCGDMWLuaPatch.d \
             OAABDataNCGDMWLuaPatch.d \
             SothaSilExpandedNCGDMWLuaPatch.d \
             TamrielDataNCGDMWLuaPatch.d \
             TelAruhnChroniclesNCGDMWLuaPatch.d \
             ThiefExperienceOverhaul1.3bTribunalCombinedNCGDMWLuaPatch.d; \
    do
        delta_plugin convert "$d"/*.yaml || echo FAILED
done
